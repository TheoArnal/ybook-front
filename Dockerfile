FROM node:18-alpine
RUN apk add --no-cache curl jq python3 py3-pip gettext
RUN pip install awscli

ENTRYPOINT [ "" ]